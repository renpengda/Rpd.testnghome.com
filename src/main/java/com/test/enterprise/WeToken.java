package com.test.enterprise;

import static io.restassured.RestAssured.given;


/**
 * @Author: Renpengda
 * @Date: 2021/1/7 17:45
 * @Description:
 */
public class WeToken {

    private static String token;

    //todo:用户获取token
    public static String getWeToken() {
        return given()
                .queryParam("mobile", WeTestConfig.getInstance().mobile)
                .queryParam("password", WeTestConfig.getInstance().password)
                .queryParam("identity", WeTestConfig.getInstance().identity)
                .queryParam("sign", WeTestConfig.getInstance().sign)
                .when()
                .get("http://contactstest.125339.com.cn/access/rest/v200/token")
                .then()
                .statusCode(200)
                .extract().path("token");


    }


    public static String getToken() {
        if (token == null) {
            token = getWeToken();
        }
        return token;
    }

}
