package com.test.enterprise.contact;

import com.test.enterprise.Restful;
import com.test.enterprise.WeToken;
import io.restassured.http.ContentType;

import static io.restassured.RestAssured.given;

/**
 * @Author: Renpengda
 * @Date: 2021/1/9 15:50
 * @Description:
 */
public class Contact extends Restful {
    public Contact() {
            reset();
    }

    public void reset() {
        requestSpecification=given()
                .log().all()
                .header("X-Token", WeToken.getToken())
                .contentType(ContentType.JSON);

//        requestSpecification.filter((req,rex,ctx)->{
//          //todo:对请求 响应做封装
//            return ctx.next(req,rex);
//        });
    }


}
