package com.test.enterprise.contact;

import com.jayway.jsonpath.JsonPath;
import com.test.enterprise.WeToken;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.util.HashMap;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.requestSpecification;

/**
 * @Author: Renpengda
 * @Date: 2021/1/8 11:26
 * @Description:
 */
public class Enterprise extends Contact{



    public Response list(String enterpriseId) {
        Response response = requestSpecification
                .when().get("http://contactstest.125339.com.cn/access/rest/v200/enterprise/" + enterpriseId)
                .then().log().all().extract().response();
        reset();
        return response;
    }

    public Response PostList(String ConTactSid, String name, String displayName, String description) {
        reset();
        String body = JsonPath.parse(this.getClass().getResourceAsStream("/data/Data.json"))
                .set("$.name", name)
                .set("$.displayName", displayName)
                .set("$.description", description)
                .jsonString();
        return requestSpecification
                .body(body)
                .when().post("http://contactstest.125339.com.cn/access/rest/v200/contacts/" + ConTactSid)
                .then().log().all()
                .statusCode(200).extract().response();
    }

    public Response PostMap(HashMap<String,Object> map){

        String body = template("/data/Data.json",map);
        reset();
        return requestSpecification.body(body)
                .when().post("http://contactstest.125339.com.cn/access/rest/v200/contacts/1250257937729941505")
                .then().log().all()
                .extract().response();
    }

}
