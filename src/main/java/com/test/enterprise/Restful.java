package com.test.enterprise;

import static io.restassured.RestAssured.given;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import io.restassured.RestAssured;
import  io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import java.util.HashMap;

/**
 * @Author: Renpengda
 * @Date: 2021/1/9 15:32
 * @Description:
 */
public class Restful {
    HashMap<String,Object> query = new HashMap<String,Object>();
    public static RequestSpecification requestSpecification = given();
    public Response send(){
        requestSpecification = given().log().all();
        query.entrySet().forEach( entry ->
                requestSpecification.queryParam(entry.getKey(),entry.getValue()));
        return requestSpecification.when().request("get","baidu.com");
    }


    public static String template(String path , HashMap<String,Object> map){
        //todo:支持从json文件读取数据并改写
        DocumentContext documentContext = JsonPath.parse(Restful.class
                .getResourceAsStream(path));
        map.entrySet().forEach(entry ->
                documentContext.set(entry.getKey(),entry.getValue()));
        return documentContext.jsonString();
    }

    public static Response templateFromHar( String path ,String pattern, HashMap<String , Object> map){
        //todo:从hr中读取请求，进行更新
        DocumentContext documentContext = JsonPath.parse(Restful.class
                .getResourceAsStream(path));
        map.entrySet().forEach(entry ->
                documentContext.set(entry.getKey(),entry.getValue()));

        String method = documentContext.read("method");
        String url = documentContext.read("url");


        return requestSpecification.when().request(method,url);

    }

}
