
import com.test.enterprise.WeToken;
import org.testng.annotations.Test;

import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
/**
 * @Author: Renpengda
 * @Date: 2021/1/7 10:57
 * @Description: Test
 */
public class GetTokenTest {

        @Test
        public void testToken(){

            String token = WeToken.getWeToken();
            assertThat(token,not(equalTo(null)));

        }

}
