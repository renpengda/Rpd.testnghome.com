package com.test.enterprise.contact;



import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.HashMap;

import static org.hamcrest.Matchers.equalTo;
import static org.testng.Assert.*;

/**
 * @Author: Renpengda
 * @Date: 2021/1/8 13:47
 * @Description:
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class EnterpriseTest {

    Enterprise enterprise;

    @BeforeAll
    void setUp() {
        if (enterprise == null) {
            enterprise = new Enterprise();
        }
    }

    @Test
    public void testList() {
        enterprise.list("1293747549911392256").then().statusCode(200).body("data.businessTypes.authority[0]", equalTo(6));
        enterprise.list("test").then().statusCode(200).body("msg", equalTo("no permission"));
        enterprise.list("12345").then().statusCode(200).body("msg", equalTo("no permission"));
    }

    @Test
    public void testPostList() {
        enterprise.PostList("1250257937729941505", "自动化测试", "党建通讯录Autotest", "接口自动化测试")
                .then().statusCode(200).body("msg", equalTo("success"));
    }

    @Test
    public void testPostMap() {
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("name", "自动化测试");
        map.put("description", "Api自动化测试");
        enterprise.PostMap(map).then().statusCode(200).body("msg", equalTo("success"));
    }
}